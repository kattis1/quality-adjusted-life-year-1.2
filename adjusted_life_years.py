import sys

result = 0
counter = 0
for line in sys.stdin:
    print(line)
    if counter == 0:
        max_num = int(line)
    elif counter <= max_num:
        logs = line.split(' ')
        result += float(logs[0]) * float(logs[1])
    counter+=1
    if counter > max_num:
        break

print(result)